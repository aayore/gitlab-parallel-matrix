# GitLab Parallel Matrix Demo

This project demonstrates the use and utility of [GitLab Parallel Matrix](https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix).  It uses [YAML anchors and aliases](https://yaml101.com/anchors-and-aliases/) in combination with Parallel Matrix to further optimize the CI configuration and reduce repeated code.


## Warnings

1. Examine the included `parameter.yml` CloudFormation template does not conflict with your existing infrastructure.
1. If your access key is set correctly (see below), SSM Parameters will be created automatically.


## Prerequisites

1. Fork this project using the `Fork` button above.
1. Add an access key to your forked project:
  If you do not yet have an access key:
    - Do not use an access key associated with a production account
    - [Click here](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-about) for more information about access keys.
    - [Creating an access key](https://aws.amazon.com/premiumsupport/knowledge-center/create-access-key/)
  - Navigate to `Settings` > `CI/CD` > `Variables` and set the following:
    - `AWS_ACCESS_KEY_ID`
    - `AWS_SECRET_ACCESS_KEY`


## Files

`parameter.yml` is an extremely simple CloudFormation template that creates a Parameter in Systems Manager.

`before.yml` is a GitLab CI configuration file that **does not** leverage Parallel Matrix.

`after.yml` is a GitLab CI configuration file that **does** not leverage Parallel Matrix.


## Instructions

1. Make a commit to see the "before" configuration in action:

  ```shell
  cp before.yml .gitlab-ci.yml
  git add .gitlab-ci.yml
  git commit -m 'use the "before" CI configuration'
  git push
  ```

  Check the [Pipelines](-/pipelines) page of your project to see the results.

1. Make a commit to see the "after" configuration in action:

  ```shell
  cp -f after.yml .gitlab-ci.yml
  git add .gitlab-ci.yml
  git commit -m 'use the "after" CI configuration'
  git push
  ```

  Check the [Pipelines](-/pipelines) page of your project to see the results.

1. Now add a `yellow` identifier to the `.prod_matrix` object, make a commit, and observe the results.